#Find the sum of all the elements in x
x= [3, 1, 2, 4, 1, 2]

print(sum(x))

#Find the length of x
print(len(x))

#Print the last three items of x
print(x[-3:])

#Print the first three items of x
print(x[:3])

#sort array
print(sorted(x))

#append 10
x.append(10)
print(x)

#Add another item 11 to x
x.append(11)
print(x)

#Remove the last item from x
x.pop()
print(x)

#How many times does 1 occur in x?
print(x.count(1))

#Check whether the element 10 is present in x
print("Yes") if 10 in x else print("no")

#add all elements of y to x
y=[5,1,2,3]
x=x+y
print(x)

#copy of x
xcopy=[*x]
print(xcopy)


