#Sum of arbitary no of inputs

def args_sum(*args):
    return sum(args)
print("sum of n number of inputs: {}\n\n".format(args_sum(1,2,3,4)))

#absolute sum of arbitary no of inputs

def args_sum_absolute(*args, **kwargs):
    if kwargs.get("absolute"):
        return(abs(sum(args)))
    else:
        return((sum(args)))
print("absolute sum: {}".format(args_sum_absolute(1,-2,-3,absolute=True)))

