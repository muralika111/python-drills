
#print all 2 digit prime numbers
def isprime(x):
    
    for i in range(2,x):
        if x%i==0:
            return False
    return True
def two_digit_prime():
    for j in range (10,100):
        if isprime(j):
            print(j,end=" ")
two_digit_prime()
    
print("\n\n-----------------------------------------------------------------------------------\n\n")


#print all n digit prime numbers

def n_digit_primes_default_2(n=2):
    start= 10**(n-1)
    stop = 10**n

    
    for j in range (start,stop):
        if isprime(j):
            print(j,end=" ")
n_digit_primes_default_2(3)


    

        
