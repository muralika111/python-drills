
#exception handling
class FilepathNotValid(FileNotFoundError):
    pass

def uniqsort(filepath):
    file_not_found = False
    try:
        file=open(filepath, "r")
        text=file.read()
        arr=text.split(" ")
        print(sorted(list(set(arr))))
        
    except FileNotFoundError:
        file_not_found = True
    if file_not_found:
        raise FilepathNotValid("file path not valid")
        
uniqsort("../sample.txt")
