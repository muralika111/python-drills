
Numbers
---------

Can you cause an integer overflow in Python? 

Ans: No, because python has arbitary precission integers
    




```python
#2
a=7/2
b=7//2
c=-7/2
d=-7//2
print(a,b,c,d)
```

    3.5 3 -3.5 -4
    

STRINGS
--------


```python
x = "one two three four"
#Print the last 3 characters of x
print(x[-3:])

```

    our
    


```python
#Print the first 10 characters of x
print(x[-10:])
```

    three four
    


```python
#Print characetrs 4 through 10 of x
print(x[4:10])
```

    two th
    


```python
#Find the length of x
print(len(x))
```

    18
    


```python
#Split x into its words
split=x.split(" ")
print(split)
```

    ['one', 'two', 'three', 'four']
    


```python
#capitalize the first character

x = "one two three four"
x=x.capitalize()
print(x)
```

    One two three four
    


```python
#convert the string to uppercase
x= "one two three four"

x=x.upper()
print(x)

```

    ONE TWO THREE FOUR
    


```python
#After executing the above code, what is the value of y?
x = "one two three four"
y = x
x = "one two three"
#
print(y)
```

    one two three four
    

LISTS
-------


```python
x = [3, 1, 2, 4, 1, 2]

```


```python
#Find the sum of all the elements in x
x= [3, 1, 2, 4, 1, 2]

print(sum(x))
```

    13
    


```python
#Find the length of x
print(len(x))
```

    6
    


```python
#Print the last three items of x
print(x[-3:])
```

    [4, 1, 2]
    


```python
#Print the first three items of x
print(x[:3])
```

    [3, 1, 2]
    


```python
#sort array
print(sorted(x))
```

    [1, 1, 2, 2, 3, 4]
    


```python
#append 10
x.append(10)
print(x)
```

    [3, 1, 2, 4, 1, 2, 10]
    


```python
#Add another item 11 to x
x.append(11)
print(x)
```

    [3, 1, 2, 4, 1, 2, 10, 11]
    


```python
#Remove the last item from x
x.pop()
print(x)
```

    [3, 1, 2, 4, 1, 2, 10]
    


```python
#How many times does 1 occur in x?
print(x.count(1))
```

    2
    


```python
#Check whether the element 10 is present in x
print("Yes") if 10 in x else print("no")
```

    Yes
    


```python
#add all elements of y to x
y=[5,1,2,3]
x=x+y
print(x)
```

    [3, 1, 2, 4, 1, 2, 10, 5, 1, 2, 3]
    


```python
#copy of x
xcopy=[*x]
print(xcopy)
```

    [3, 1, 2, 4, 1, 2, 10, 5, 1, 2, 3]
    


```python
#print all 2 digit prime numbers
def isprime(x):
    
    for i in range(2,x):
        if x%i==0:
            return False
    return True
for j in range (10,100):
    if isprime(j):
        print(j,end=" ")
    

        

```

    11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 


```python
#print all n digit prime numbers
def n_digit_primes_default_2(n=2):
    ilimit= 10**(n-1)
    limit = 10**n

    
    for j in range (ilimit,limit):
        if isprime(j):
            print(j,end=" ")
n_digit_primes_default_2(3)
    

        

```

    no of digits3
    101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379 383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479 487 491 499 503 509 521 523 541 547 557 563 569 571 577 587 593 599 601 607 613 617 619 631 641 643 647 653 659 661 673 677 683 691 701 709 719 727 733 739 743 751 757 761 769 773 787 797 809 811 821 823 827 829 839 853 857 859 863 877 881 883 887 907 911 919 929 937 941 947 953 967 971 977  


```python
#count no of repetation of words in a string
s=input("enter the string")
obj={}
lst=s.split(" ")


for i in lst:
    obj.setdefault(i,0)
    obj[i]+=1
print("------------------------------\n\n")
print(obj)


```

    enter the stringPython is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems. Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported.
    ------------------------------
    
    
    {'Python': 3, 'is': 2, 'an': 1, 'interpreted,': 1, 'high-level,': 1, 'general-purpose': 1, 'programming': 4, 'language.': 2, 'interpreters': 1, 'are': 2, 'available': 1, 'for': 1, 'many': 1, 'operating': 1, 'systems.': 1, 'a': 1, 'multi-paradigm': 1, 'Object-oriented': 1, 'and': 1, 'structured': 1, 'fully': 1, 'supported.': 1}
    


```python
#function to print all items of a dictionary
 
def print_dict_keys_and_values(d):
    print (d.items())
print_dict_keys_and_values(obj)
```

    dict_items([('Python', 3), ('is', 2), ('an', 1), ('interpreted,', 1), ('high-level,', 1), ('general-purpose', 1), ('programming', 4), ('language.', 2), ('interpreters', 1), ('are', 2), ('available', 1), ('for', 1), ('many', 1), ('operating', 1), ('systems.', 1), ('a', 1), ('multi-paradigm', 1), ('Object-oriented', 1), ('and', 1), ('structured', 1), ('fully', 1), ('supported.', 1)])
    


```python
#Sum of arbitary no of inputs

def args_sum(*args):
    lst=[*args]
    return sum(lst)
args_sum(1,2,3,4)
```




    10




```python
#absolute sum of arbitary no of inputs

def args_sum_absolute(*args, **kargs):
    for (ey,value)in kargs.items():
        if value==True:
            print(abs(sum([*args])))
        else:
            print((sum([*args])))
args_sum_absolute(1,-2,-3,kargs=True)
```

    4
    


```python
#Save the first n natural numbers and their squares into a file in the csv format.
file=open("data.csv", "a")
def numbers_and_squares(n, filepath):
    file=open(filepath, "a")
    for i in range(1,n):
        file.write("{},{}\n".format(i,i**2))
    file.close()
n=int(input("enter n"))

numbers_and_squares(n,"data.csv")

```

    enter n10
    


```python
#create a file and write some dummy text
file=open("sample.txt", "w")
file.write("hello this is the sample text file containing some sample text for your sample usage of the sample code")
file.close()

```


```python
# A function to perform "uniq <file_path> | sort"
def uniqsort(filepath):
    file=open(filepath, "r")
    text=file.read()
    arr=text.split("\n")
    print(sorted(list(set(arr))))
uniqsort("sample.txt")


    
```

    ['code.', 'containing', 'file', 'for', 'hello', 'is', 'of', 'sample', 'some', 'text', 'the', 'this', 'usage', 'your']
    


```python
#exception handling
class FilepathNotValid(FileNotFoundError):
    pass

def uniqsort(filepath):
    file_not_found = False
    try:
        file=open(filepath, "r")
        text=file.read()
        arr=text.split(" ")
        print(sorted(list(set(arr))))
        
    except FileNotFoundError:
        file_not_found = True
    if file_not_found:
        raise FilepathNotValid("file path not valid")
        
uniqsort("../sample.txt")
```

    FilepathNotValid
    
