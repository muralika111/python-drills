#Save the first n natural numbers and their squares into a file in the csv format.

def numbers_and_squares(n, filepath):
    file=open(filepath, "a")
    for i in range(1,n):
        file.write("{},{}\n".format(i,i**2))
    file.close()
n=int(input("enter n"))

numbers_and_squares(n,"data.csv")