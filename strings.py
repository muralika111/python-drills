
x = "one two three four"
#Print the last 3 characters of x
print(x[-3:])


#Print the first 10 characters of x
print(x[-10:])

#Print characetrs 4 through 10 of x
print(x[4:10])

#Find the length of x
print(len(x))

#Split x into its words
split=x.split(" ")
print(split)

#capitalize the first character

x = "one two three four"
x=x.capitalize()
print(x)

#convert the string to uppercase
x= "one two three four"

x=x.upper()
print(x)

#After executing the above code, what is the value of y?
x = "one two three four"
y = x
x = "one two three"
print(y)

    
